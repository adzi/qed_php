<?php
session_start();
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
include("dbhandlerbook.php");
$isbn=$isbn_err=$avaliable_err="";
$avaliable="";
if($_SERVER["REQUEST_METHOD"] == "POST"){

	if(empty(trim($_POST["isbn"]))){$isbn_err="error";}
		elseif(strlen(trim($_POST["isbn"])) < 1){
        $isbn_err = "name must have1 char";
    } else{
        $isbn = trim($_POST["isbn"]);
    }
	if(empty(trim($_POST["avaliable"]))){$isbn_err="error";}
elseif(strlen(trim($_POST["avaliable"])) < 1){
        $isbn_err = "name must have1 char";
    } else{
        $isbn = trim($_POST["avaliable"]);
    } 
if(empty($isbn_err)&&empty($avaliable_err)){
	 //$avaliable=44;
	//$isbn="ffvfvkk"; 
$sql = "UPDATE users SET avaliable=? WHERE users.isbn=?";

 if($stmt = mysqli_prepare($link, $sql)){
           
           mysqli_stmt_bind_param($stmt,"si", $avaliable, $isbn );
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
				
                
				
                header("location: changeavaliable.php");
				exit(); 
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
mysqli_stmt_close($stmt); }}

 mysqli_close($link);

}

?>

<!DOCTYPE html>
<html lang="en">
<body>
 <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"> 
            <div class="form-group <?php echo (!empty($isbn_err)) ? 'has-error' : ''; ?>">
                <label>isbn</label>
                <input type="name" name="isbn" class="form-control" value="<?php echo $isbn; ?>">
               <span class="help-block"><?php echo $isbn_err; ?></span>
            </div>
			<div class="form-group <?php echo (!empty($avaliable_err)) ? 'has-error' : ''; ?>">
                <label>avaliable</label>
                <input type="number" name="avaliable" class="form-control" value="<?php echo $avaliable; ?>">
               <span class="help-block"><?php echo $avaliable_err; ?></span>
            </div>
           
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-link" href="welcome.php">Cancel</a>
            </div>
        </form>
</body>