<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
 require_once "dbhandler.php";
 $new_username= $new_usernam_err="";
 if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    
     if(empty(trim($_POST["new_username"]))){
        $new_usernam_err = "Please enter the name.";     
    } elseif(strlen(trim($_POST["new_username"])) < 1){
        $new_usernam_err = "name must have1 char";
    } else{
        $new_username = trim($_POST["new_username"]); 
    }
	if(empty($new_usernam_err)){
		  $sql = "SELECT id FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["new_username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This username is already taken.";
					echo $username_err;
                } else{
        
        $sql = "UPDATE users SET username = ? WHERE id = ?"; 
        
        if($stmt = mysqli_prepare($link, $sql)){
            
           mysqli_stmt_bind_param($stmt,"si", $new_username, $_SESSION["id"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
				
                // naem updated successfully. Destroy the session, and redirect to login page
				 session_destroy();
                header("location: login.php");
				exit(); 
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
   mysqli_stmt_close($stmt);
				}}
    }
 
    // Close connection
 mysqli_close($link);}}}
?>
	
	<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Reset Name</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
       
    </style>
</head>

<body>
    
        <h2>Reset name</h2>
        <p>Please fill out this form to reset your name.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"> 
            <div class="form-group <?php echo (!empty($new_usernam_err)) ? 'has-error' : ''; ?>">
                <label>New name</label>
                <input type="name" name="new_username" class="form-control" value="<?php echo $new_username; ?>">
                <span class="help-block"><?php echo $new_usernam_err; ?></span>
            </div>
           
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <a class="btn btn-link" href="welcome.php">Cancel</a>
            </div>
        </form>
 
</body>
 
</html> 